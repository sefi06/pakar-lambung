<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/login', function () {
//     return view('login');
// });


Auth::routes();
Route::middleware(['guest_pasien'])->group(function () {
    Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
    Route::get('/password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('/password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'resetPassword'])->name('password.update');
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::any('/info', [App\Http\Controllers\HomeController::class, 'info'])->name('info');

Route::middleware(['auth:pasien'])->group(function () {
    Route::any('/konsultasi', [App\Http\Controllers\HomeController::class, 'konsultasi'])->name('konsultasi');
});
