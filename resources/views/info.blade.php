@extends('layouts.app')

@section('content')
      <section class="py-xxl-10 pb-0" id="home">
        <div class="bg-holder bg-size" style="background-image:url(assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;"></div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-12 col-xxl-12 text-center align-item-center text-md-start">
              <h2 class="fw-bold text-dark mb-4 mt-4 mt-lg-0">Informasi Tentang Penyakit Lambung</h2>
              <p class="text-dark fw-bold">Penyakit asam lambung terjadi ketika otot kerongkongan bagian bawah 
(otot LES) melemah, otot ini seharusnya berkontraksi dan menutup 
saluran ke kerongkongan setelah makanan turun ke lambung.Bila otot ini 
lemah, kerongkongan akan tetap terbuka dan asam lambung akan naik 
kembali ke kerongkongan.</p>
              <p class="text-dark fw-bold">Gejala penyait lambung yang paling umum adalah heartburn atau sensasi 
terbakar di dada, rasanya seperti nyeri dada terbakar yang dimulai 
dari belakang tulang dada dan bergerak ke atas ke leher dan 
tenggorokan. Makanan seperti masuk kembali ke mulut sehingga 
meninggalkan rasa asam atau pahit, heartburn biasanya terjadi setelah 
makan, yang bisa lebih buruk di malam hari atau saat berbaring, setiap 
makanan yang kita konsumsi akan masuk ke lambung dan dicerna secara 
kimiawi. Proses pencernaan ini dibantu oleh enzim pepsin dan renin 
yang bercampur dengan asam lambung (HCl). Jika terjadi gangguan, 
mukosa akan rusak dan menimbulkan rasa sakit atau nyeri, apabila 
gangguan ini terus-menerus terjadi, asam lambung akan memecah mukosa 
dan menyebabkan iritasi dan peradangan. Kondisi inilah yang 
mengakibatkan lambung terasa sakit. Rasa nyeri disebabkan oleh asam 
lambung yang bersentuhan dengan lapisan mukosa. Akibatnya, ujung-ujung 
saraf menjadi lebih peka oleh rasa nyeri.</p>
            </div>
          </div>
        </div>
      </section>
      @include('layouts/footer')
@endsection
