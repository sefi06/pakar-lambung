@extends('layouts.app')

@section('css')
<style>
  @media print {
    @page {
        margin: 0;
        padding: 0;
    }
    body, html {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
    }
    body * {
      visibility: hidden;
    }
    #resep-obat, #resep-obat * {
      visibility: visible;
    }
    #resep-obat {
        display: block!important;
        position: absolute;
        left: 0;
        top: 0;
        margin: 0;
        padding: 5%;
        border: 1px solid black;
        width: 100%;
        height: 100%;
    }
  }
  .text-dark{
    color: black!important;
  }
</style>
@endsection



@section('javascript')
<script>
    document.getElementById('print-btn').addEventListener('click', function() {
      window.print();
    });
</script>
@endsection

@section('content')
      <section class="py-xxl-10 pb-0" id="home">
        <div class="bg-holder bg-size" style="background-image:url(assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
            @if($mode =='input')
            <div class="row min-vh-xl-100 min-vh-xxl-25">
                <div class="col-lg-12 z-index-2">
                    <form id="regForm" style="border: 5px solid;" method="post">
                        @csrf
                        <div class="all-steps" id="all-steps" style="display: none!important;"> 
                            <span class="step"></span> 
                            <span class="step"></span> 
                        </div>
                        <!-- <div class="tab">
                            <h3>Masukkan Data Diri</h3>
                            <br>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input class="form-control form-livedoc-control mb-3 req" name="nama" type="text"/>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label">Umur</label>
                                <div class="col-sm-9">
                                    <input class="form-control form-livedoc-control mb-3 req" name="umur" type="number" />
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-9">
                                    <select class="form-select req" name="kelamin">
                                        <option value="L">L</option>
                                        <option value="P">P</option>
                                    </select>
                                </div>
                            </div>
                        </div> -->
                        <div class="tab">
                            <h3>Pilih gejala yang dialami</h3>
                            <br>
                            <div class="mb-3 row d-flex">
                                @foreach($gejala as $key => $value)
                                    <div class="form-check col-12 d-flex">
                                        <div class="col-6">
                                            <!-- <input class="form-check-input gejala-option" type="checkbox" name="gejala[]" value="{{$key}}" id="check_{{$key}}"> -->
                                            <label class="form-check-label" for="check_{{$key}}">
                                                {{$value}}
                                            </label>
                                        </div>

                                        <div class="col-6">
                                            <select class="form-select ms-2 select-kondisi" name="bobot_user[]" id="select_{{$key}}">
                                                <option value="0">Tidak</option>
                                                <option value="1.0">Pasti</option>
                                                <option value="0.8">Hampir Pasti</option>
                                                <option value="0.6">Kemungkinan Besar</option>
                                                <option value="0.4">Mungkin</option>
                                                <option value="0.2">Tidak Tahu</option>
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div style="overflow:auto;" id="nextprevious">
                            <div style="float:right;"> 
                                <button class="btn btn-sm btn-outline-secondary rounded-pill order-1 order-lg-0 ms-lg-4" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button> 
                                <button class="btn btn-sm btn-outline-primary rounded-pill order-1 order-lg-0 ms-lg-4" type="button" id="nextBtn" onclick="nextPrev(1)">Next</button> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @else
            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-lg-8 z-index-2" id="hasil-konsultasi">
                    <h3 class="text-center">Hasil Konsultasi</h3>
                    <form id="regForm" style="border: 5px solid;" method="post">
                        @csrf
                        <h3>Hasil Analisa</h3>
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label fw-bold">Penyakit</label>
                            <div class="col-sm-7">
                                <label class="col-sm-4 col-form-label fw-bold">Hasil</label>
                            </div>
                        </div>
                        <hr>
                        @foreach($penyakit_all as $key => $value)
                        <div class="mb-2 row">
                            <label class="col-sm-5 col-form-label">{{$value['nama']}}</label>
                            <div class="col-sm-7">
                                <label class="col-sm-4 col-form-label">: {{ isset($result[$key]) ? $result[$key] ."%" : "0%" }}</label>
                            </div>
                        </div>
                        @endforeach
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label fw-bold">Kesimpulan</label>
                        </div>
                        <hr>
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Nama</label>
                            <div class="col-sm-7">
                                <label class="col-sm-12 col-form-label">: {{$nama}}</label>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Tanggal Konsultasi</label>
                            <div class="col-sm-7">
                                <label class="col-sm-12 col-form-label">: {{$tanggal->format('d-m-Y')}}</label>
                            </div>
                        </div>
                        <!-- <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Umur</label>
                            <div class="col-sm-7">
                                <label class="col-sm-12 col-form-label">: </label>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-7">
                                <label class="col-sm-12 col-form-label">: </label>
                            </div>
                        </div> -->
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Penyakit Yang Diderita</label>
                            <div class="col-sm-7">
                                <label class="col-sm-12 col-form-label">: {{$penyakit['nama']}}</label>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-5 col-form-label">Solusi</label>
                            <div class="col-sm-7">
                                <p class="col-sm-12 col-form-label">{!!nl2br($penyakit['solusi'])!!}</p>
                            </div>
                        </div>
                    </form>
                    <div class="col-12 mt-5 d-flex align-items-center justify-content-center">
                        <!-- <div class="col-3"> -->
                            <a class="btn btn-sm btn-outline-primary rounded-pill order-1 order-lg-0" id="btn-konsultasi" href="{{ route('konsultasi') }}">Konsultasi Ulang</a>
                        <!-- </div> -->
                        <button class="btn btn-sm btn-outline-secondary rounded-pill order-1 order-lg-0 ms-lg-4" id="print-btn" type="button">Print Hasil Konsultasi</button>
                        <!-- <div class="col-3">
                            <a class="btn btn-sm btn-primary rounded-pill order-1 order-lg-0" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div> -->
                    </div>
                </div>
            </div>
            @endif
        </div>
      </section>
      @if($mode !='input')
        <div class="row text-dark d-none" id="resep-obat">
            <div class="container">
                <div class="header text-center mt-2 mb-5 pb-3" style="border-bottom: 1px solid black;">
                    <div class="col-12 d-flex">
                        <div class="col-auto text-end"">
                            <div class="border border-3 border-black rounded-circle text-center p-2" style="width: 105px;">
                                <i style="font-size: 84px;" class="fas fa-plus-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto ms-2 mt-3">
                            <label>KLINIK DAN APOTEK GUNUNGSARI</label><br>
                            <i class="text-dark">Jl. Raya Gunungsari, RT.03/RW.01, Sukagumiwang, Indramayu, Jawa Barat 45274</i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="text-center text-dark"><u>SALINAN RESEP</u></label>
                </div>
                <div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-bold">Hasil Analisa</label>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-bold">Penyakit</label>
                        <div class="col-sm-7">
                            <label class="col-sm-4 col-form-label fw-bold">Hasil</label>
                        </div>
                    </div>
                    @foreach($penyakit_all as $key => $value)
                    <div class="mb-2 row">
                        <label class="col-sm-5 col-form-label fw-normal">{{$value['nama']}}</label>
                        <div class="col-sm-7">
                            <label class="col-sm-4 col-form-label fw-normal">: {{ isset($result[$key]) ? $result[$key] ."%" : "0%" }}</label>
                        </div>
                    </div>
                    @endforeach
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-bold">Kesimpulan</label>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-normal">Nama</label>
                        <div class="col-sm-7">
                            <label class="col-sm-12 col-form-label fw-normal">: {{$nama}}</label>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-normal">Tanggal Diagnosa</label>
                        <div class="col-sm-7">
                            <label class="col-sm-12 col-form-label fw-normal">: {{$tanggal->format('d-m-Y')}}</label>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-normal">Penyakit Yang Diderita</label>
                        <div class="col-sm-7">
                            <label class="col-sm-12 col-form-label fw-normal">: {{$penyakit['nama']}}</label>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-5 col-form-label fw-normal">Solusi</label>
                        <div class="col-sm-7">
                            <p class="col-sm-12 col-form-label fw-normal">{!!nl2br($penyakit['solusi'])!!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      @endif
      @include('layouts/footer')
@endsection