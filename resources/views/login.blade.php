<!doctype html>
<html lang="en">
  <head>
  	<title>Login 10</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="assets/css/login.css">

	</head>
	<body class="img js-fullheight" style="background-image:url(assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;">
	<section class="ftco-section d-flex align-items-center justify-content-center h-100">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
                    <div class="login-wrap p-0">
                        <h3 class="mb-4 text-center text-info"><b>Login</b></h3>
                        <form action="#" class="signin-form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                            <input id="password-field" type="password" class="form-control" placeholder="Password" required>
                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control btn btn-primary bg-primary submit px-3">Login</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <!-- <div class="w-50 text-md-right">
                                    <a href="#" style="color: #fff">Forgot Password</a>
                                </div> -->
                            </div>
                        </form>
                        <p class="w-100 text-center"><strong>&mdash; Belum memiliki akun? <a href="/register">Register</a>&mdash;</strong></p>
                    </div>
                </div>
			</div>
		</div>
	</section>

	</body>
</html>

