@extends('layouts.app')

<?php

if(auth('pasien')->check()) {
  $py ='py-6';
}
else {
  $py ='py-0';
}

?>

@section('content')
      <section class="py-xxl-10 pb-0" id="home">
        <div class="bg-holder bg-size" style="background-image:url(assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row min-vh-xl-100 min-vh-xxl-25">
            <div class="col-md-5 col-xl-6 col-xxl-7 order-0 order-md-1 text-end"><img class="pt-7 pt-md-0 w-70" src="assets/img/gallery/hero-5.png" alt="hero-header" /></div>
            <div class="col-md-75 col-xl-6 col-xxl-5 text-md-start text-center {{$py}}">
              <h1 class="fw-light font-base fs-6 fs-xl-6 text-start">@if(auth('pasien')->check()) SELAMAT DATANG DI @endif<br /><strong>SISTEM PAKAR DIAGNOSA</strong><br /><strong>PENYAKIT LAMBUNG</strong></h1>
              <p class="fs-1 mb-5">Konsultasikan Penyakit Anda Sekarang!</p>
              @if(auth('pasien')->check())
                <a class="btn btn-lg btn-primary rounded-pill" href="/konsultasi" role="button">Konsultasi Sekarang</a>
              @else
                <a class="btn btn-lg btn-primary rounded-pill" href="/register" role="button">Buat Akunku</a>
              @endif
            </div>
          </div>
        </div>
      </section>
      @include('layouts/footer')
@endsection
