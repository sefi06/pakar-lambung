<section class="py-0 bg-primary">

    <div class="container">
        <div class="row justify-content-md-between justify-content-evenly text-light py-4">
            <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
                ©Informatika 2024
            </div>
            <div class="col-12 col-sm-8 col-md-6 text-end">
                Hilda Amalia Febriani
            </div>
        </div>
    </div>
    <!-- end of .container-->

</section>