<?php

$route  = \Illuminate\Support\Facades\Route::currentRouteName();

switch ($route) {
    case 'login':
        $title  = 'Login';
        break;
    case 'register':
        $title  = 'Register';
        break;
    case 'password.reset':
        $title  = 'Reset Password';
        break;
    case 'info':
        $title  = 'Info Penyakit';
        break;
    case 'konsultasi':
        $title  = 'Konsultasi';
        break;
    default:
        $title  = 'Home';
        break;
}

?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="h-100">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>Sistem Pakar | {{$title}}</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicons/favicon-16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicons/favicon.ico">
    <link rel="manifest" href="/assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.png">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="/assets/css/theme.css" rel="stylesheet" />
    <link href="/assets/css/style.css" rel="stylesheet">
    @yield('css')
  </head>


  <body class="h-100">

    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main h-100" id="top">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
            <div class="container">
                <a class="navbar-brand text-primary" href="/">
                    <img width="120px" src="/assets/img/logonew.png" width="118" alt="logo" />
                    <!-- {{ config('app.name', 'Laravel') }} -->
                </a>
                @if(\Illuminate\Support\Facades\Route::currentRouteName() != 'login' && \Illuminate\Support\Facades\Route::currentRouteName() != 'register' && \Illuminate\Support\Facades\Route::currentRouteName() != 'password.reset')
                <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto pt-2 pt-lg-0 font-base">
                            <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="/">Home</a></li>
                            <li class="nav-item px-2"><a class="nav-link" href="/info">Info Penyakit</a></li>
                            <!-- <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="/konsultasi">Konsultasi</a></li> -->
                            @if(auth('pasien')->check())
                                <li class="nav-item px-2">
                                    <a class="btn btn-sm btn-primary rounded-pill order-1 order-lg-0" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            @else
                                <li class="nav-item px-2">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-primary rounded-pill order-1 order-lg-0" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                            {{ __('Login') }}
                                        </a>

                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                            <li><a class="dropdown-item" href="/admin">{{ __('Login Admin') }}</a></li>
                                        </ul>
                                    </div>
                                    <!-- <a class="btn btn-sm btn-primary rounded-pill order-1 order-lg-0" href="{{ route('login') }}">
                                        {{ __('Login') }}
                                    </a> -->
                                </li>
                            @endif
                    </ul>
                </div>
                @endif
            </div>
        </nav>
        @yield('content')
    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="/assets/js/@popperjs/popper.min.js"></script>
    <script src="/assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/is/is.min.js"></script>
    <script src="https://scripts.sirv.com/sirvjs/v3/sirv.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src="/assets/js/fontawesome/all.min.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/main.js"></script>
    @yield('javascript')

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&amp;family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100&amp;display=swap" rel="stylesheet">
  </body>

</html>