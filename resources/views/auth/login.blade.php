@extends('layouts.app')

@section('content')

<section class="ftco-section d-flex align-items-center justify-content-center h-100 img js-fullheight" style="background-image:url(assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h3 class="mb-4 text-center text-info"><b>Login</b></h3>
                    @if(session('success'))
                        <p style="color: green;">{{ session('success') }}</p>
                    @endif
                    <form method="POST" action="" class="signin-form">
                        @csrf
                        <div class="form-group mb-3">
                            <div class="col-md-12">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="Username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <button type="submit" class="form-control btn btn-primary bg-primary submit px-3">Login</button>
                        </div>
                    </form>
                    <p class="w-100 text-center mt-3"><strong>&mdash; Belum memiliki akun? <a href="/register">Register</a>&mdash;</strong></p>
                    <p class="w-100 text-center mt-3"><strong><a class="text-info" href="/password/reset">Lupa Password</a></strong></p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
