@extends('layouts.app')

@section('content')

<section class="ftco-section d-flex align-items-center justify-content-center h-100 img js-fullheight" style="background-image:url(/assets/img/gallery/hero-bg.png);background-position:top center;background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h3 class="mb-4 text-center text-info"><b>Reset Password</b></h3>
                    <form method="POST" action="" class="signin-form">
                        @csrf
                        <div class="form-group mb-3">
                            <div class="col-md-12">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="off" placeholder="Username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off" placeholder="Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="off" placeholder="Konfirmasi Password">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <button type="submit" class="form-control btn btn-primary bg-primary submit px-3">Reset</button>
                        </div>
                    </form>
                    <p class="w-100 text-center mt-3"><strong>&mdash; Sudah memiliki akun? <a href="/login">Login</a>&mdash;</strong></p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
