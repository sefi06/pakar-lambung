<?php

namespace App\Filament\Widgets;

use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\RiwayatKonsultasi;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class AdminWidgets extends BaseWidget
{
    protected function getCards(): array
    {
        return [
            Card::make('Total Gejala', Gejala::count())->icon('heroicon-o-switch-vertical'),
            Card::make('Total Penyakit', Penyakit::count())->icon('heroicon-o-switch-vertical'),
            Card::make('Total Konsultasi', RiwayatKonsultasi::count())->icon('heroicon-o-switch-vertical'),
        ];
    }
}
