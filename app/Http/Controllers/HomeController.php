<?php

namespace App\Http\Controllers;

use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\RiwayatKonsultasi;
use App\Models\Rules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function info()
    {
        return view('info');
    }

    public function konsultasi(Request $request)
    {
        $gejala = Gejala::all()->pluck('nama', 'id');

        if (request()->isMethod('get')) {
            return view('konsultasi', [
                'mode'      => 'input',
                'gejala'    => $gejala
            ]);
        } else {
            $nama   = auth('pasien')->user()->name;
            $gejala = array_keys($gejala->toArray());
            $bobot_user = $request->bobot_user;

            $rules  = Rules::all()->pluck(null, 'gejala_id')->toArray();
            
            $datas  = [];

            for ($i=0; $i < count($gejala) ; $i++) {
                $datas[]    = $rules[$gejala[$i]];
                $datas[$i]['bobot_user']    = floatval($bobot_user[$i]);
            }

            /**
             * group by id penyakit
             */
            $groupedDatas = collect($datas)->groupBy('penyakit_id')->toArray();

            /**
             * perhitungan nilai CF
             */
            foreach ($groupedDatas as $penyakit_id => $data) {
                foreach ($data as $key => $value) {
                    $groupedDatas[$penyakit_id][$key]['cf'] = $groupedDatas[$penyakit_id][$key]['bobot_pakar'] * $groupedDatas[$penyakit_id][$key]['bobot_user'];
                }
            }
            /**
             * perhitungan combine
             */
            $combines   = [];
            foreach ($groupedDatas as $penyakit_id => $data) {
                $combines[$penyakit_id] = $this->Combines($data);
            }

            /**
             * perhitungan presentase
             */
            $result = [];
            foreach ($combines as $penyakit_id => $data) {
                $result[$penyakit_id] = round($data * 100);
            }

            $presentase = max($result);
            $penyakit_id= array_search($presentase, $result);

            $konsultasi = new RiwayatKonsultasi();

            $konsultasi->nama       = $nama;
            // $konsultasi->umur       = 0;
            // $konsultasi->jenis_kelamin  = '-';
            $konsultasi->pasien_id  = Auth::guard('pasien')->id();
            $konsultasi->penyakit_id= $penyakit_id;
            $konsultasi->presentase = $presentase.'%';

            $konsultasi->save();

            $penyakit   = Penyakit::all()->pluck(null, 'id')->toArray();

            return view('konsultasi', [
                'mode'      => 'result',
                'result'        => $result,
                'nama'          => $nama,
                'penyakit'      => $penyakit[$penyakit_id],
                'penyakit_all'  => $penyakit,
                'tanggal'       => Carbon::now()
            ]);
        }
    }

    private function Combines($data) {
        $cumulativeResult = 0.0;

        foreach ($data as $item) {
            $cumulativeResult = $cumulativeResult + $item['cf'] * (1 - $cumulativeResult);
        }

        return $cumulativeResult;
    }
}
