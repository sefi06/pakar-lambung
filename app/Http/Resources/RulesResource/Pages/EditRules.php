<?php

namespace App\Http\Resources\RulesResource\Pages;

use App\Http\Resources\RulesResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditRules extends EditRecord
{
    protected static string $resource = RulesResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
