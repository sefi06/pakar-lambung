<?php

namespace App\Http\Resources\RulesResource\Pages;

use App\Http\Resources\RulesResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListRules extends ListRecords
{
    protected static string $resource = RulesResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
