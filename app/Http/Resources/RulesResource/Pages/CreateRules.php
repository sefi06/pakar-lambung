<?php

namespace App\Http\Resources\RulesResource\Pages;

use App\Http\Resources\RulesResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateRules extends CreateRecord
{
    protected static string $resource = RulesResource::class;
}
