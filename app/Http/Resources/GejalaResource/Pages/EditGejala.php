<?php

namespace App\Http\Resources\GejalaResource\Pages;

use App\Http\Resources\GejalaResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditGejala extends EditRecord
{
    protected static string $resource = GejalaResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
