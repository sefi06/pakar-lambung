<?php

namespace App\Http\Resources\GejalaResource\Pages;

use App\Http\Resources\GejalaResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGejala extends CreateRecord
{
    protected static string $resource = GejalaResource::class;

    protected static bool $canCreateAnother = false;
}
