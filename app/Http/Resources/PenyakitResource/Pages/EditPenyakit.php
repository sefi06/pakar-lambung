<?php

namespace App\Http\Resources\PenyakitResource\Pages;

use App\Http\Resources\PenyakitResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditPenyakit extends EditRecord
{
    protected static string $resource = PenyakitResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
