<?php

namespace App\Http\Resources\PenyakitResource\Pages;

use App\Http\Resources\PenyakitResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListPenyakits extends ListRecords
{
    protected static string $resource = PenyakitResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
