<?php

namespace App\Http\Resources\PenyakitResource\Pages;

use App\Http\Resources\PenyakitResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreatePenyakit extends CreateRecord
{
    protected static string $resource = PenyakitResource::class;
}
