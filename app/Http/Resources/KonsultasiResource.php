<?php

namespace App\Http\Resources;

use App\Http\Resources\KonsultasiResource\Pages;
use App\Http\Resources\KonsultasiResource\RelationManagers;
use App\Models\RiwayatKonsultasi;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class KonsultasiResource extends Resource
{
    protected static ?string $model = RiwayatKonsultasi::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function canCreate(): bool
    {
        return false;
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('index')->rowIndex()->label('No'),
                Tables\Columns\TextColumn::make('nama')->searchable()->label('Nama User'),
                Tables\Columns\TextColumn::make('created_at')->searchable()->label('Tanggal Diagnosa'),
                Tables\Columns\TextColumn::make('penyakit.nama')->searchable()->label('Penyakit yang dialami'),
                Tables\Columns\TextColumn::make('presentase')->searchable()->label('Presentase Penyakit'),
                // Tables\Columns\TextColumn::make('penyakit.solusi')->searchable()->label('Solusi'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                //
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListKonsultasis::route('/'),
            'create' => Pages\CreateKonsultasi::route('/create'),
            // 'edit' => Pages\EditKonsultasi::route('/{record}/edit'),
        ];
    }    
}
