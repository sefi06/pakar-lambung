<?php

namespace App\Http\Resources;

use App\Http\Resources\RulesResource\Pages;
use App\Http\Resources\RulesResource\RelationManagers;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\Rules;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class RulesResource extends Resource
{
    protected static ?string $model = Rules::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    protected static ?string $navigationLabel = 'Basis Aturan';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('penyakit_id')
                    ->required()
                    ->searchable()
                    ->label('Penyakit')
                    ->options(Penyakit::all()->pluck('nama', 'id')),
                Forms\Components\Select::make('gejala_id')
                    ->required()
                    ->searchable()
                    ->label('Gejala')
                    ->unique(ignorable: fn ($record) => $record)
                    ->options(Gejala::all()->pluck('nama', 'id')),
                Forms\Components\TextInput::make('bobot_pakar')
                    ->numeric()
                    ->required(),
                // Forms\Components\TextInput::make('bobot_user')
                //     ->numeric()
                //     ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('penyakit.nama')->searchable(),
                Tables\Columns\TextColumn::make('gejala.nama')->searchable(),
                Tables\Columns\TextColumn::make('bobot_pakar')->searchable(),
                // Tables\Columns\TextColumn::make('bobot_user')->searchable()
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                // Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListRules::route('/'),
            'create' => Pages\CreateRules::route('/create'),
            'edit' => Pages\EditRules::route('/{record}/edit'),
        ];
    }    
}
