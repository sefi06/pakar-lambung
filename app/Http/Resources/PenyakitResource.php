<?php

namespace App\Http\Resources;

use App\Http\Resources\PenyakitResource\Pages;
use App\Http\Resources\PenyakitResource\RelationManagers;
use App\Models\Penyakit;
use Closure;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class PenyakitResource extends Resource
{
    protected static ?string $model = Penyakit::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('kode')
                    ->required()
                    ->unique(ignorable: fn ($record) => $record)
                    ->maxLength(32),
                TextInput::make('nama')
                    ->required()
                    ->label('Nama Penyakit')
                    ->maxLength(255),
                // Select::make('jenis_solusi')
                //     ->options([
                //         'text' => 'Text',
                //         'image' => 'Gambar',
                //     ])
                //     ->default('image')
                //     ->label('Jenis Solusi')
                //     ->required(),
                // FileUpload::make('solusi')
                //     ->image(),
                Textarea::make('solusi')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('kode')->searchable(),
                Tables\Columns\TextColumn::make('nama')->searchable(),
                // Tables\Columns\ImageColumn::make('solusi')->height(100)->label('Gambar Solusi'),
                Tables\Columns\TextColumn::make('solusi')->searchable()->label('Solusi'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([

            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPenyakits::route('/'),
            'create' => Pages\CreatePenyakit::route('/create'),
            'edit' => Pages\EditPenyakit::route('/{record}/edit'),
        ];
    }    
}
