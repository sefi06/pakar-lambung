<?php

namespace App\Http\Resources\KonsultasiResource\Pages;

use App\Http\Resources\KonsultasiResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateKonsultasi extends CreateRecord
{
    protected static string $resource = KonsultasiResource::class;
}
