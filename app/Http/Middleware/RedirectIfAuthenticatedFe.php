<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedFe
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (auth('pasien')->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
