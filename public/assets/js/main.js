var currentTab = 0;

document.addEventListener("DOMContentLoaded", function (event) {
    showTab(currentTab);
});

function showTab(n) {
    var x = document.getElementsByClassName("tab");

    // Check if the index is within the valid range
    if (n < 0 || n >= x.length) {
        console.error("Invalid tab index:", n);
        return;
    }

    // Hide all tabs
    for (var i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }

    // Show the specified tab
    x[n].style.display = "block";

    // Update navigation buttons
    if (n === 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }

    if (n === x.length - 1) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
}

function nextPrev(n) {
    var x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;

    // Ensure that the next tab index is within the valid range
    if (currentTab + n >= 0 && currentTab + n < x.length) {
        x[currentTab].style.display = "none";
        currentTab = currentTab + n;
        showTab(currentTab);
    } else if (currentTab + n === x.length) {
        // If this is the last step, check and submit the form
        checkAndSubmitForm();
    }
}


function validateForm() {
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByClassName("req");

    for (i = 0; i < y.length; i++) {
        if (y[i].value == "") {
            y[i].className += " invalid";
            valid = false;
        }
    }
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}

function checkAndSubmitForm() {
    var selectElements = document.querySelectorAll('.select-kondisi');
    var selectedCount = 0;

    selectElements.forEach(function(selectElement) {
        if (selectElement.value > 0) {
            selectedCount++;
        }
    });

    if (selectedCount < 2) {
        alert("Silahkan pilih minimal 2 gejala.");
        return false;
    } else {
        document.getElementById("regForm").submit();
    }
}
