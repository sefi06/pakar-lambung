<?php

return [

    'confirmation' => 'Apakah Anda yakin ingin menghapus ini?',

    'actions' => [

        'cancel' => [
            'label' => 'Batal',
        ],

        'confirm' => [
            'label' => 'Konfirmasi',
        ],

        'submit' => [
            'label' => 'Kirim',
        ],

    ],

];
