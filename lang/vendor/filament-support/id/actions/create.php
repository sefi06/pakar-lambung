<?php

return [

    'single' => [

        'label' => 'Tambah',

        'modal' => [

            'heading' => 'Buat :label',

            'actions' => [

                'create' => [
                    'label' => 'Tambah',
                ],

                'create_another' => [
                    'label' => 'Tambah & tambah lainnya',
                ],

            ],

        ],

        'messages' => [
            'created' => 'Data berhasil dibuat',
        ],

    ],

];
