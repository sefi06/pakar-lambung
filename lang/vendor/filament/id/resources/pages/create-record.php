<?php

return [

    'title' => 'Buat :label',

    'breadcrumb' => 'Tambah',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'Batal',
            ],

            'create' => [
                'label' => 'Tambah',
            ],

            'create_another' => [
                'label' => 'Tambah & tambah lainnya',
            ],

        ],

    ],

    'messages' => [
        'created' => 'Data berhasil dibuat',
    ],

];
